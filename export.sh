#!/bin/bash


cp $HOME/.config/i3/config ./.config/i3/config
cp $HOME/.config/nvim/init.vim ./.config/nvim/init.vim
cp $HOME/.config/ranger/rc.conf ./.config/ranger/rc.conf
cp $HOME/.config/gtk-3.0/settings.ini ./.config/gtk-3.0/settings.ini
cp $HOME/.config/zathura/zathurarc ./.config/zathura/zathurarc
cp $HOME/.config/alacritty/alacritty.yml ./.config/alacritty/alacritty.yml
cp $HOME/.config/picom.conf ./.config/picom.conf
cp $HOME/.zshrc ./.zshrc
cp $HOME/.tmux.conf ./.tmux.conf
cp $HOME/.bashrc ./.bashrc
